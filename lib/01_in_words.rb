ONE_TO_NINE = {
	0 => "zero",
	1 => "one",
	2 => "two",
	3 => "three",
	4 => "four",
	5 => "five",
	6 => "six",
	7 => "seven",
	8 => "eight",
	9 => "nine"
}

TEN_TO_NINETEEN = {
	10 => "ten",
	11 => "eleven",
	12 => "twelve",
	13 => "thirteen",
	14 => "fourteen",
	15 => "fifteen",
	16 => "sixteen",
	17 => "seventeen",
	18 => "eighteen",
	19 => "nineteen"
}

TWENTY_TO_NINETY = {
	20 => "twenty",
	30 => "thirty",
	40 => "forty",
	50 => "fifty",
	60 => "sixty",
	70 => "seventy",
	80 => "eighty",
	90 => "ninety"
}

MULTS_OF_HUNDRED = {
	100 => "hundred",
	1000 => "thousand",
	1000000 => "million",
	1000000000 => "billion",
	1000000000000 => "trillion"
}

class Fixnum

	def in_words
		if self < 10
			ONE_TO_NINE[self]
		elsif self < 20
			TEN_TO_NINETEEN[self]
		elsif self < 100
			tens_word = TWENTY_TO_NINETY[(self / 10) * 10]
			if self % 10 != 0
				tens_word + " " + (self % 10).in_words
			else
				tens_word
			end
		else
			num_remainder = MULTS_OF_HUNDRED.keys.take_while {|num_remainder| num_remainder <= self}.last
			number_words = (self/num_remainder).in_words + " " + MULTS_OF_HUNDRED[num_remainder]
			if self % num_remainder != 0
				number_words + " " + (self % num_remainder).in_words
			else
				number_words
			end
		end
	end

end
